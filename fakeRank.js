const Eris = require('eris')
const config = require('./config')

const client = new Eris.Client(config.token)

const RESPONSE = '🚫 You aren\'t ranked yet. Send some messages first, then try again.'
const RESPONSE_USER = member => `🚫 **${member.username}** isn\'t ranked yet.`

client.once('ready', () => {
  console.log(`Connected to ${client.user.username}#${client.user.discriminator}`)
})

client.on('messageCreate', async message => {
  if (message.channel.guild?.id !== config.guildId) return
  const guild = client.guilds.get(config.guildId)

  const args = message.content.split(/ +/)
  const cmd = args.shift().match(new RegExp(`^${config.prefix}(\\w+)$`))?.[1]

  switch (cmd) {
    case 'rank': {
      console.log(`#${message.channel.name} > ${message.author.username}#${message.author.discriminator} > ${message.content}`)
      const [ search ] = args
      if (typeof search === 'string') {
        const searchId = search.match(/\d+/)?.[0]
        if (searchId) {
          if (searchId === message.author.id)
            return message.channel.createMessage(RESPONSE)
          const member = (await guild.fetchMembers({
            userIDs: [searchId]
          }))?.[0]
          if (member)
            return message.channel.createMessage(RESPONSE_USER(member))
        }
        return message.channel.createMessage({
          embed: {
            color: 0xF04848,
            description: [
              'Invalid command usage, try using it like:',
              `\`${config.prefix}rank (optional member)\`\n`,
              'Arguments:',
              '`member`: *User mention (@User) or user ID (159985870458322944)*'
            ].join('\n')
          }
        })
      }
      message.channel.createMessage(RESPONSE)
      break
    }
  }
})

client.connect()

process.on('uncaughtException', function(err) {
  console.log('\nCaught exception: ' + err);
})
