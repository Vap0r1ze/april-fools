const db = require('quick.db')
const memberHandler = require('./memberHandler')
const config = require('./config')

const savedMembers = db.get(`${config.guildId}:savedMembers`)

const isUnique = (e, i, a) => a.indexOf(e) === i

memberHandler({
  jobName: 'restore',
  filterMembers: members => members.filter(member => savedMembers.some(savedMember => {
    return savedMember.id === member.id && savedMember.roles.some(id => !member.roles.includes(id))
  })),
  sortMembers: members => members.sort((a, b) => {
    const m1 = savedMembers.find(savedMember => savedMember.id === a.id)
    const m2 = savedMembers.find(savedMember => savedMember.id === b.id)
    const p1 = config.roles.findIndex(id => m1.roles.includes(id))
    const p2 = config.roles.findIndex(id => m2.roles.includes(id))
    return p1 - p2
  }),
  processBucket: bucket => {
    const promiseBucket = []
    for (const member of bucket) {
      const rolesToRestore = savedMembers.find(savedMember => savedMember.id === member.id).roles
      promiseBucket.push(member.edit({ roles: member.roles.concat(...rolesToRestore).filter(isUnique) }))
    }
    return promiseBucket
  }
})

process.on('uncaughtException', function(err) {
  console.log('\nCaught exception: ' + err);
})
