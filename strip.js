const db = require('quick.db')
const memberHandler = require('./memberHandler')
const config = require('./config')

memberHandler({
  jobName: 'strip',
  filterMembers: members => members.filter(member => config.roles.some(roleId => member.roles.includes(roleId))),
  sortMembers: members => members.sort((a, b) => {
    const p1 = config.roles.findIndex(id => a.roles.includes(id))
    const p2 = config.roles.findIndex(id => b.roles.includes(id))
    return p2 - p1
  }),
  processBucket: bucket => {
    const promiseBucket = []
    for (const member of bucket) {
      const rolesToSave = member.roles.filter(id => config.roles.includes(id))
      const postRoles = member.roles.filter(id => !config.roles.includes(id))
      db.push(`${config.guildId}:savedMembers`, { id: member.id, roles: rolesToSave })
      promiseBucket.push(member.edit({ roles: postRoles }))
    }
    return promiseBucket
  }
})

process.on('uncaughtException', function(err) {
  console.log('\nCaught exception: ' + err);
})
