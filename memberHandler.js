const Eris = require('eris')
const config = require('./config')

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

module.exports = ({ jobName, filterMembers, sortMembers, processBucket }) => {
  const client = new Eris.Client(config.token)

  client.once('ready', async () => {
    console.log(`Connected to ${client.user.username}#${client.user.discriminator}`)

    console.log('Fetching members')
    const guild = client.guilds.get(config.guildId)
    await guild.fetchMembers({
      presences: true
    })
    console.log(`Fetched ${guild.members.size} members`)

    const roledMembers = sortMembers(filterMembers(guild.members))
    console.log(`Found ${roledMembers.length} members to ${jobName}`)

    const onlineRoledMembers = []
    const offlineRoledMembers = []
    for (const member of roledMembers) {
      if (member.status === 'dnd' || member.status === 'idle' || member.status === 'online') {
        onlineRoledMembers.push(member)
      } else {
        offlineRoledMembers.push(member)
      }
    }
    console.log(`(${onlineRoledMembers.length} online, ${offlineRoledMembers.length} offline)`)

    if (roledMembers.length >= 100 && !config.force) {
      console.log('ATTEMPTED ON LARGE SERVER, TRY AGAIN WITH config.forge SET TO true')
      process.exit(0)
    }

    const totalBucketCount = Math.ceil(roledMembers.length / config.bucketSize)
    console.log('')
    for (let i = 0; i < totalBucketCount; i++) {
      process.stdout.write(`\rProcessing bucket ${i+1}/${totalBucketCount}`)

      const bucket = []
      if (offlineRoledMembers.length)
        bucket.push(...offlineRoledMembers.splice(0, config.bucketSize))
      if (bucket.length < config.bucketSize)
        bucket.push(...onlineRoledMembers.splice(0, config.bucketSize - bucket.length))

      const promiseBucket = processBucket(bucket)

      if (i === totalBucketCount - 1)
        await Promise.all(promiseBucket)
      else
        await Promise.all([...promiseBucket, sleep(config.bucketDelay)])
    }
    console.log(`\nProcessed ${totalBucketCount} buckets!`)

    client.disconnect()
  })

  client.connect()
}
