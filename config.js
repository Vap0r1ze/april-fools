module.exports = {
  token: 'Bot Token',
  prefix: '!',
  guildId: '123456789012345678',
  bucketSize: 10,
  bucketDelay: 10e3,
  roles: [
    '123456789012345678', // Highest Rank
    '123456789012345678', // Middle Rank
    '123456789012345678', // Middle Rank
    '123456789012345678', // Lowest Rank
  ],
  force: false,
}
